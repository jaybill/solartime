var RouteHandler = require('react-router').RouteHandler;
var React = require('react/addons');
var mixins = require('baobab-react').mixins;

var Root = React.createClass({
  mixins: [mixins.root],
  render() {
    return <RouteHandler {...this.props} />;
  }
});

module.exports = Root;
