'use strict';

var SolartimeApp = require('./SolartimeApp');
var Root = require('./Root');
var React = require('react');
var Router = require('react-router');
var Route = Router.Route;
var stateTree = require('../lib/StateTree');
var log = require('loglevel');


var content = document.getElementById('content');

var Routes = (

<Route name="/root" handler={Root}>
        <Route name="/" handler={SolartimeApp}/>
</Route>

);

Router.run(Routes, function(Handler, state) {
  log.setLevel('DEBUG');
  React.render(<Handler tree={stateTree} {...state}/>, content);
});
