'use strict';

var React = require('react/addons');
var Input = require('react-bootstrap').Input;
var Button = require('react-bootstrap').Button;
var Grid = require('react-bootstrap').Grid;
var Row = require('react-bootstrap').Row;
var Col = require('react-bootstrap').Col;
var Table = require('react-bootstrap').Table;
var Panel = require('react-bootstrap').Panel;
var CursorMixin = require('../lib/CursorMixin');
var Actions = require('../lib/Actions');
var DateTimeField = require('react-bootstrap-datetimepicker');
var moment = require('moment');
//var log = require('loglevel');

require('../styles/main.css');

var SolartimeApp = React.createClass({
  mixins: [CursorMixin],
  cursors: {
    solartimes: ['solartimes']
  },

  updateLocation() {

    var location = this.refs.location.getValue();
    if (location) {
      var start = this.refs.start.getValue();
      var end = this.refs.end.getValue();
      Actions.loadSolarData(location, start, end);
    }
  },

  selectDay(day) {
    this.setState({
      selectedDay: day
    });
  },


  getInitialState() {

    var start = moment().subtract(6, 'day');

    return {
      start: start
    };
  },

  render() {
    var st;

    if (this.state && this.state.solartimes && this.state.solartimes.length) {
      var detail;
      var rows = [];
      for (var r in this.state.solartimes) {

        rows.push(<tr key={r} onClick={this.selectDay.bind(this, r)}>
                    <td>{this.state.solartimes[r].date}</td>
                    <td>{this.state.solartimes[r].sunrise}</td>
                    <td>{this.state.solartimes[r].sunset}</td>
                    <td>{this.state.solartimes[r].day_length}</td>
                    </tr>);

      }

      if (this.state.selectedDay) {
        var sday = this.state.solartimes[this.state.selectedDay];

        var title = moment(sday.date, 'YYYY-MM-DD').format('dddd, MMMM Do, YYYY');

        detail = <Panel header={title}>
              <Row>
              <Col md={6}>
              <dl>
              <dt>Solar Noon</dt>
              <dd>{sday.solar_noon}</dd>
              <hr/>
              <dt>Civil Twilight Start</dt>
              <dd>{sday.civil_twilight_begin}</dd>
              <hr/>
              <dt>Nautical Twilight Start</dt>
              <dd>{sday.nautical_twilight_begin}</dd>
              <hr/>
              <dt>Astronomical Twilight Start</dt>
              <dd>{sday.astronomical_twilight_begin}</dd>

              </dl>
              </Col>
              <Col md={6}>
              <dl>
              <dt>&nbsp;</dt>
              <dd>&nbsp;</dd>
              <hr/>
              <dt>Civil Twilight End</dt>
              <dd>{sday.civil_twilight_end}</dd>

              <hr/>

              <dt>Nautical Twilight End</dt>
              <dd>{sday.nautical_twilight_end}</dd>
              <hr/>

              <dt>Astronomical Twilight End</dt>
              <dd>{sday.astronomical_twilight_end}</dd>

              </dl>
              </Col>
          </Row>

             </Panel>;
      }

      st = <Row><Col md={6}><Table striped bordered condensed hover>
            <thead>
            <tr>
            <th>Date</th>
            <th>Sunrise</th>
            <th>Sunset</th>
            <th>Day Length</th>
        </tr></thead>
            <tbody>{rows}</tbody>
            </Table></Col>
            <Col md={6}>
            {detail}
            </Col>
        </Row>;

    }

    return (
      <div className="main">
        <div className="container">
            <Grid>
            <Row>
            <Col md={12}>
            <h1>Sunrise/Sunset Times</h1>
            </Col>
            </Row>
            <Row>
            <Col md={4}>
            <Input ref="location" type="text"
      placeholder="Enter Location"></Input>
            </Col>
            <Col md={2}>
            <DateTimeField
      ref="start"
      dateTime={this.state.start}></DateTimeField>
            </Col>
            <Col md={2}>
            <DateTimeField
      ref="end"></DateTimeField>
            </Col>
            <Col md={2}>
            <Button bsStyle="primary"
      onClick={this.updateLocation}>
            Show Solar Time</Button>
            </Col>
            </Row>
            {st}
        </Grid>
        </div>
      </div>
      );
  }
});

module.exports = SolartimeApp;
