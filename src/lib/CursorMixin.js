var mixins = require('baobab-react').mixins;
var _ = require('lodash');

var CursorMixin = {

  mixins: [mixins.branch],

  componentWillMount() {
    var keys = _.keys(this.cursors);
    var self = this;
    _.map(keys, function(k) {
      if (typeof self.state[k] === 'undefined') {
        throw new Error('Cursor points to a leaf in the state tree that is not defined.');
      }
    });
  }
};
export default CursorMixin;
