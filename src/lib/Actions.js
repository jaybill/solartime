var log = require('loglevel');
var popsicle = require('popsicle');
var URI = require('URIjs');
var stateTree = require('../lib/StateTree');
var moment = require('moment');
var Actions = {

  loadSolarData: function(geoquery, start, end) {
    if (start > end) {
      var originalStart = start;
      start = end;
      end = originalStart;
    }

    start = start / 1000;
    end = end / 1000;
    var days = [];

    var current = moment(start, 'X');

    do {
      days.push(current.format('YYYY-MM-DD'));
      current = moment(current, 'X').add(1, 'day');
    } while (current.unix() <= end);

    log.debug('Days', days);

    var solartimes = stateTree.select('solartimes');
    var uri = new URI('https://maps.googleapis.com/maps/api/geocode/json');
    uri.query({
      address: geoquery
    });

    popsicle(uri.toString())
      .then(function(res) {
        log.debug(res);
        if (res.body.status === 'OK') {
          return res.body.results[0].geometry.location;
        }
      }).then(function(location) {
      log.debug(location);

      var solarOptions = {
        lat: location.lat,
        lng: location.lng
      };
      var solarUri = new URI('http://api.sunrise-sunset.org/json');
      var promises = [];

      for (var day in days) {
        solarOptions.date = days[day];
        solarUri.query(solarOptions);
        promises.push(popsicle(solarUri.toString()));
      }
      return promises;

    }).then(function(promises) {

      Promise.all(promises).then(function(data) {
        var rows = [];
        for (var i in data) {
          if (data[i].body.status === 'OK') {
            var row = data[i].body.results;
            row.date = days[i];
            rows.push(row);
          }
        }
        solartimes.set(rows);
      });
    });
  }
};
export default Actions;
