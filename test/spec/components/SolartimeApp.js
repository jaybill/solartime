'use strict';

describe('SolartimeApp', () => {
  let React = require('react/addons');
  let SolartimeApp, component;

  beforeEach(() => {
    let container = document.createElement('div');
    container.id = 'content';
    document.body.appendChild(container);

    SolartimeApp = require('components/SolartimeApp.js');
    component = React.createElement(SolartimeApp);
  });

  it('should create a new instance of SolartimeApp', () => {
    expect(component).toBeDefined();
  });
});
